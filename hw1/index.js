class Employee {
    constructor(name, age, salary){
        this._name = name;        
        this._age = age;
        this._salary = salary;
    }
    
    set objName(newName){
        this._name= newName;
    }
    get objName(){
        return this._name;
    }

    set objAge(newAge){
        this._age= newAge;
    }
    get objAge(){
        return this._age;
    }

    set objSalary(newSalary){
        this._salary= newSalary;
    }
    get objSalary(){
        return this._salary;
    }

}

const employer = new Employee('Дима', '25', '1000');
console.log(`Изначальная зп: ${employer._salary}$`);

employer.objSalary = '5000'
console.log(`Изменённая зп: ${employer._salary}$`);

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this._lang = lang
    }

    set objSalary(salary){
        super._salary = salary;
    }
    get objSalary(){
        return this._salary *= 3;
    }
}

const programmer1 = new Programmer('Олег', '26', '100000', 'С#');
const programmer2 = new Programmer('Вася', '50', '2000', 'java');
const programmer3 = new Programmer('Петя', '60', '3000', 'js');

console.log(`Увеличенная в 3 раза зп: ${programmer1.objSalary}$`);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);