const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

const root = document.createElement('div');
const ul = document.createElement('ul');
root.setAttribute("id", "root");

document.body.prepend(root);
root.prepend(ul);

for (let i = 0; i < books.length; i++) {
    try{
        if (books[i].author && books[i].name && books[i].price) {
            books[i] = Object.values(books[i]).join();
    
            let li = document.createElement('li');
            li.innerText = books[i];
            ul.prepend(li);
        } else {
            throw new Error(`property was missed`);
        }
    } catch(err){
        if (err.message === `property was missed`){
            !books[i].author? console.log(`missed property is: author`) :
            !books[i].name? console.log(`missed property is: name`) :
            !books[i].price? console.log(`missed property is: price`) : 
            null;
        }
    }
}